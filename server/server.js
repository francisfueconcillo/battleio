/**
 * Created with JetBrains PhpStorm.
 * User: root
 * Date: 5/31/14
 * Time: 4:26 PM
 * To change this template use File | Settings | File Templates.
 */
var cloak = require('cloak'),
    cloakUtils = new (require('./utils').cloakUtils)(),
    _ = require('underscore'),
    Game = require('./game').Game,
    publicMessages = [
        {type: 'system', text: 'Welcome to the Game of the Generals! #enjoy', from: 'GG' }
    ];

cloak.configure({
    port: 8090,

    // 3 hour room life
    roomLife: 1000 * 60 * 60 * 3,

    autoJoinLobby: true,
    minRoomMembers: 1,
    pruneEmptyRooms: 1000,
    reconnectWait: 3000,

    messages: {
        registerUsername: function (arg, user) {
            var users = cloak.getUsers();
            var username = arg.username;
            var usernames = _.pluck(users, 'username');
            var success = false;
            if (_.indexOf(usernames, username) === -1) {
                success = true;
                user.name = username;
            }
            user.message('registerUsernameResponse', success);
            cloakUtils.refreshRoomMembersLobby(cloak, user);
        },

        getCurrentPlayer: function (arg, user) {
            user.message('getCurrentPlayerResponse', {
                id: user.id,
                name: user.name
            });
        },

        joinLobby: function (arg, user) {
            cloak.getLobby().addMember(user);
            user.message('joinLobbyResponse');
        },

        listUsers: function (arg, user) {
            cloakUtils.refreshUserLobby(user);
        },

        listRooms: function (arg, user) {
            var rooms = cloak.getRooms(true);
            user.message('listRooms', cloakUtils.getAvailableRooms(rooms));
        },

        createRoom: function (arg, user) {
            var room = cloak.createRoom(arg.name, 2),
                success = room.addMember(user);

            //initialize game
            room.data = {
                game: new Game()
            };
            room.data.game.initBoard({id: user.id, name: user.name});

            user.message('roomCreated', {
                success: success,
                room: cloakUtils.getUserRoomInfo(user.id, room)
            });

            var rooms = cloak.getRooms(true);
            cloak.messageAll('listRooms', cloakUtils.getAvailableRooms(rooms));
        },

        joinRoom: function (room, user) {
            var roomObject = cloak.getRoom(room.id),
                opponent = cloak.getUser(roomObject.data.game.player1.id);

            roomObject.addMember(user);
            roomObject.data.game.playerTwoJoined({id: user.id, name: user.name});

            user.message('joinRoomResponse', {
                success: true,
                room: cloakUtils.getUserRoomInfo(user.id, roomObject)
            });

            opponent.message('opponentJoinRoom', {
                success: true,
                room: cloakUtils.getUserRoomInfo(opponent.id, roomObject)
            });

            var rooms = cloak.getRooms(true);
            cloak.messageAll('listRooms', cloakUtils.getAvailableRooms(rooms));
        },

        leaveRoom: function (arg, user) {
            user.leaveRoom();
        },

        publicMessage: function (arg, user) {
            var message = arg.message,
                formattedMessage = {type: 'public', text: message, from: user.name};

            publicMessages.push(formattedMessage);
            cloak.messageAll('publicMessageResponse', formattedMessage);
        },

        challengeUser: function (arg, user) {
            var targetId = arg.targetId,
                targetUser = cloak.getUser(targetId),
                room = cloak.createRoom(cloakUtils.generateRoomName(user.name), 2),
                success = room.addMember(user);

            if (targetUser) {
                //initialize game
                room.data = {
                    game: new Game()
                };
                room.data.game.initBoard({id: user.id, name: user.name});

                user.message('roomCreated', {
                    success: success,
                    room: cloakUtils.getUserRoomInfo(user.id, room)
                });

                targetUser.message('challengeUser', {
                    id: user.id,
                    name: user.name,
                    room: {
                        id: room.id,
                        name: room.name
                    }
                });
            }
        },

        challengeReject: function (arg, user) {
            var targetId = arg.targetId,
                roomId = arg.roomId,
                targetUser = cloak.getUser(targetId);

            if (targetUser) {
                targetUser.message('challengeReject', {
                    id: user.id,
                    name: user.name,
                    roomId: roomId
                });
            }
        },

        privateMessage: function (arg, user) {
            var targetId = arg.targetId,
                message = arg.message,
                targetUser = cloak.getUser(targetId);

            if (targetUser) {
                targetUser.message('privateMessage', {
                    id: user.id,
                    name: user.name,
                    message: message
                });
            }
        },

        playerReady: function (arg, user) {
            var roomObject = user.getRoom(),
                opponent = cloak.getUser(roomObject.data.game.getOpponent(user.id).id);

            roomObject.data.game.playerReady(user.id);

            user.message('updateGame', {
                room: cloakUtils.getUserRoomInfo(user.id, roomObject)
            });

            if (opponent) {
                opponent.message('updateGame', {
                    room: cloakUtils.getUserRoomInfo(opponent.id, roomObject)
                });
            }
        },

        playerMove: function (arg, user) {
            var from = arg.from,
                to = arg.to,
                roomObject = user.getRoom(),
                opponent = cloak.getUser(roomObject.data.game.getOpponent(user.id).id),
                result;

            result = roomObject.data.game.playerMove(user.id, from, to);

            if (result.success) {
                user.message('updateGame', {
                    room: cloakUtils.getUserRoomInfo(user.id, roomObject)
                });

                if (opponent) {
                    opponent.message('updateGame', {
                        room: cloakUtils.getUserRoomInfo(opponent.id, roomObject)
                    });
                }

                if (roomObject.data.game.status === 'gameOver') {
                    var winner =  roomObject.data.game.wHoWins(),
                        message;

                    if (winner) {
                        message = {
                            type: 'system',
                            text: winner.win.name + ' wins against ' + winner.lose.name + '.',
                            from: 'GG'};

                        publicMessages.push(message);
                        cloak.messageAll('publicMessageResponse', message);
                    }
                }
            }
        }
    },

    room: {
        init: function() {
            console.log('created room ' + this.id);
            this.data.lastReportedAge = 0;
        },

        newMember: function(user) {
            console.log(" new member join the room: ", user.name);
        },

        memberLeaves: function(user) {
            this.delete();

            console.log(" user leaves the room: ", user.name);
            user.room.messageMembers('leaveRoomResponse');
            cloakUtils.refreshRoomMembersLobby(cloak, user);
        },

        pulse: function() {
            // add timed turn stuff here
        },

        close: function() {
            this.messageMembers('you have left room' + this.name);
        }

    },


    lobby: {
        init: function() {
            console.log('created lobby ' + this.id);
            this.data.lastReportedAge = 0;
        },

        newMember: function(user) {
            console.log(" new member join the lobby: ", user.name);
            cloakUtils.refreshRoomMembersLobby(cloak, user);
            user.message('allPublicMessages', publicMessages);
            var rooms = cloak.getRooms(true);
            user.message('listRooms', cloakUtils.getAvailableRooms(rooms));
        },

        memberLeaves: function(user) {
            // if we have 0 people in the room, close the room
            if (this.getMembers().length <= 0) {
                this.delete();
            }
            console.log(" user leaves the lobby: ", user.name);
        },

        pulse: function() {
            // add timed turn stuff here
        },

        close: function() {
            this.messageMembers('you have left lobby ' + this.name);
        }

    }

});



cloak.run();

