/**
 * Created with JetBrains PhpStorm.
 * User: root
 * Date: 6/8/14
 * Time: 11:27 AM
 * To change this template use File | Settings | File Templates.
 */

var _ = require('underscore');

exports.Game = function () {
    var me = this,
        PIECES = [
            {name: 'General', type: 'gen-5star', rank: 150},
            {name: 'General', type: 'gen-4star', rank: 140},
            {name: 'Lt.Gen', type: 'gen-lt', rank: 130},
            {name: 'Maj.Gen', type: 'gen-maj', rank: 120},
            {name: 'Brig.Gen', type: 'gen-bri', rank: 110},
            {name: 'Colonel', type: 'col', rank: 100},
            {name: 'Lt.Col', type: 'col-lt', rank: 90},
            {name: 'Major', type: 'maj', rank: 80},
            {name: 'Captain', type: 'capt', rank: 70},
            {name: '1st-Lt', type: 'lt-1st', rank: 60},
            {name: '2nd-Lt', type: 'lt-2nd', rank: 50},
            {name: 'Sergent', type: 'sgt', rank: 40},
            {name: 'Private', type: 'pvt', rank: 30},
            {name: 'Private', type: 'pvt', rank: 30},
            {name: 'Private', type: 'pvt', rank: 30},
            {name: 'Private', type: 'pvt', rank: 30},
            {name: 'Private', type: 'pvt', rank: 30},
            {name: 'Private', type: 'pvt', rank: 30},
            {name: 'Spy', type: 'spy', rank: 200},
            {name: 'Spy', type: 'spy', rank: 200},
            {name: 'Flag', type: 'flag', rank: 1}
        ],
        gameBoard = [],
        currentTurn = null;

    me.player1 = { id: null, name: null, status: null };
    me.player2 = { id: null, name: null, status: null };
    me.status = null;



    var shuffleArray = function (o) {
        //http://stackoverflow.com/questions/2450954/how-to-randomize-shuffle-a-javascript-array
        for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
        return o;
    };

    var initPlayerBoard = function (playerType) {
        var playerPieces = shuffleArray(_.clone(PIECES)),
            i = 3;   // start in 4th element, for symmetric

        if (playerType === 'player1') {
            i = 48;
        }

        for (; i < playerPieces.length+i; i++) {
            gameBoard[i] = {
                location: i,
                owner: playerType,
                content: playerPieces.pop()
            };
        }
    };

    me.initBoard = function (player1) {
        //initialize 9x8 board in 1d array
        for (var i=0; i < 72; i++){
            gameBoard[i] = {
                location: i,
                owner: null,
                content: null
            };
        }

        me.player1.id = player1.id;
        me.player1.name = player1.name;
        me.player1.status = 'arrangePieces';
        me.status = 'waitingForOpponent';

        me.player2.name = 'Waiting...';

        initPlayerBoard('player1');
    };


    me.playerTwoJoined = function (player2) {
        me.player2.id = player2.id;
        me.player2.name = player2.name;
        me.player2.status = 'arrangePieces';
        me.status = 'waitingForReady';

        initPlayerBoard('player2');
    };

    me.isPlayerOne = function(id) {
        return  (me.player1.id === id);
    };


    var getMaskedBoard = function(playerType){
        var playerBoard = _.map(gameBoard, _.clone);  //deep copy gameBoard

        for (var i=0; i < playerBoard.length; i++) {
            if (playerBoard[i].owner !== playerType) {
                playerBoard[i].content = null;
            }
        }

        return playerBoard;
    };

    var getPlayerById = function (playerId) {
        var player = { id: null, name: null, status: null };

        if (me.player1.id === playerId) {
            player = me.player1;
        } else if (me.player2.id === playerId) {
            player = me.player2;
        }

        return player;
    };


    me.getPlayerBoard = function (playerId) {
        var board = [],
            playerType = me.getPlayerType(playerId);

        if (me.status === 'gameOver') {
            board = _.map(gameBoard, _.clone);
        } else if (playerType) {
            board = getMaskedBoard(playerType);
        }

        if (!me.isPlayerOne(playerId)) {
            board = board.reverse();
        }

        return board;
    };

    me.getPlayerStatuses = function (playerId) {
        var player = getPlayerById(playerId);

        return {
            game: me.status,
            player: player.status
        };
    };

    me.getPlayers = function () {
        return {
            player1: me.player1,
            player2: me.player2
        };
    };

    me.getPlayerType = function (playerId) {
        var playerType = null;

        if (me.isPlayerOne(playerId)) {
            playerType = 'player1';
        } else if (me.player2.id === playerId) {
            playerType = 'player2';
        }

        return playerType;
    };


    me.playerReady = function(playerId){
        if (me.status === 'inGame') { return; }


        if (me.player1.id === playerId){
            me.player1.status = 'waitingForOpponentToReady';
        } else if (me.player2.id === playerId) {
            me.player2.status = 'waitingForOpponentToReady';
        }

        if (me.player1.status === 'waitingForOpponentToReady' && me.player2.status === 'waitingForOpponentToReady') {
            me.status = 'inGame';
            setCurrentTurn(me.player1.id);
        }

    };

    me.isPlayerTurn = function (playerId) {
        return (currentTurn === playerId);
    };

    me.getOpponent = function (playerId) {
        var opponent = { id: null, name: null, status: null };

        if (me.player1.id === playerId){
            opponent = me.player2;
        } else if (me.player2.id === playerId) {
            opponent = me.player1;
        }
        return opponent;
    };

    var isValidArrangeMove = function (playerType, destination) {
        var allowedStart, allowedEnd;
        if (playerType === 'player2') {
            allowedStart = 0;
            allowedEnd = 23;
        } else if (playerType === 'player1') {
            allowedStart = 48;
            allowedEnd = 71;
        } else { return false;}

        return (destination.location >= allowedStart && destination.location <= allowedEnd);
    };

    var possibleNextLocations = function (index) {
        var possibleLocations = [];

        if (index%8 === 0) {
            possibleLocations.push(index+1);
            possibleLocations.push(index+8);
            possibleLocations.push(index-8);
        } else if((index+1)%8 === 0) {
            possibleLocations.push(index-1);
            possibleLocations.push(index+8);
            possibleLocations.push(index-8);
        } else  {
            possibleLocations.push(index+1);
            possibleLocations.push(index-1);
            possibleLocations.push(index+8);
            possibleLocations.push(index-8);
        }

        return possibleLocations;
    };

    var isValidInGameMove = function (source, destination) {
        var isNextLocationValid = (_.indexOf(possibleNextLocations(source.location), destination.location) === -1),
            hasSameOwner = (source.owner === destination.owner);

        return (!isNextLocationValid && !hasSameOwner);
    };

    var arrangeMove = function (source, destination) {
        var tmp;

        if (!isValidArrangeMove(source.owner, destination)) {
            return {success: false, msg: 'invalid move'};
        }

        if (destination.content) {  //not empty
            //swap
            tmp = _.clone(destination);
            destination.owner = source.owner;
            destination.content = source.content;

            source.owner = tmp.owner;
            source.content = tmp.content;

        } else {
            destination.owner = source.owner;
            destination.content = source.content;
            source.owner = null;
            source.content = null;
        }
        return { success: true };
    };


    var setCurrentTurn = function (playerId) {
        var playerType = me.getPlayerType(playerId);

        if (playerType === 'player1') {
            me.player1.status = 'playerTurn';
            me.player2.status = 'opponentTurn';
        } else if (playerType === 'player2') {
            me.player2.status = 'playerTurn';
            me.player1.status = 'opponentTurn';
        }

        currentTurn = playerId;
    };

    me.challengeMove = function (source, destination) {
        var isBothFlags = (source.content.rank === 1 && destination.content.rank === 1),
            isSpyVsPrivate = ((source.content.rank === 200 && destination.content.rank === 30) ||
                              (source.content.rank === 30 && destination.content.rank === 200));

        if (isBothFlags) {

            //source won
            destination.owner = source.owner;
            destination.content = source.content;

            source.owner = null;
            source.content = null;

        } else if (isSpyVsPrivate) {

            if (source.content.rank === 30) {
                //source won
                destination.owner = source.owner;
                destination.content = source.content;

                source.owner = null;
                source.content = null;
            } else {
                source.owner = null;
                source.content = null;
            }

        } else {

            if (source.content.rank === destination.content.rank) {
                //both lose
                destination.owner = null;
                destination.content = null;
                source.owner = null;
                source.content = null;
            } else if (source.content.rank < destination.content.rank) {
                //source defeated
                source.owner = null;
                source.content = null;

            } else if (source.content.rank > destination.content.rank) {
                //source won

                destination.owner = source.owner;
                destination.content = source.content;

                source.owner = null;
                source.content = null;
            }
        }
    };


    var isInOpponentHome = function (tile) {
        var playerType = tile.owner,
            location = tile.location,
            opponentRow = {};

        if (playerType === 'player1') {
            opponentRow = { start: 0, end: 7};
        } else if (playerType === 'player2') {
            opponentRow = { start: 64, end: 71};
        }

        return (location >= opponentRow.start &&  location <= opponentRow.end);
    };

    me.wHoWins = function () {
        var winner;
        if (me.player1.status ===  'gameOverWin') {
            winner = {};
            winner.win = me.player1;
            winner.lose = me.player2;

        } else if (me.player2.status === 'gameOverWin') {
            winner = {};
            winner.win = me.player2;
            winner.lose = me.player1;
        }
        return winner;
    };


    var checkGameOver = function () {
        //check if flag is still alive
        //check if flag is in last row
        var checkFlagAlive = {
            player1: null,
            player2: null
        };

        var checkFlagTouchDown = {
            player1: null,
            player2: null
        };


        for(var i=0; i < gameBoard.length; i++) {
            if (gameBoard[i].content && gameBoard[i].content.hasOwnProperty('type')) {
                if (gameBoard[i].content.type === 'flag') {
                    checkFlagAlive[gameBoard[i].owner] = true;
                    checkFlagTouchDown[gameBoard[i].owner] = isInOpponentHome(gameBoard[i]);
                }
            }
        }

        //check is player1 wins
        if ((checkFlagAlive.player1 && !checkFlagAlive.player2) || checkFlagTouchDown.player1) {
            me.player1.status = 'gameOverWin';
            me.player2.status = 'gameOverLose';
            me.status = 'gameOver';
        }

        //check is player2 wins
        if ((!checkFlagAlive.player1 && checkFlagAlive.player2) || checkFlagTouchDown.player2) {
            me.player2.status = 'gameOverWin';
            me.player1.status = 'gameOverLose';
            me.status = 'gameOver';
        }

    };

    var inGameMove = function (playerId, source, destination) {
        if (!me.isPlayerTurn(playerId)) {
            return {success: false, msg: 'not yet your turn'};
        }

        if (!isValidInGameMove(source, destination)) {
            return {success: false, msg: 'invalid move'};
        }

        if (destination.content) {
            me.challengeMove(source, destination);
        } else {
            //empty, just move
            destination.owner = source.owner;
            destination.content = source.content;
            source.owner = null;
            source.content = null;
        }
        setCurrentTurn(me.getOpponent(playerId).id)
        checkGameOver();
        return {success: true, msg: 'move done.'};
    };

    me.playerMove = function (playerId, from, to) {
        //make sure locations are valid
        if (from < 0 || from > 71 || to < 0 || to > 71) {
            return {success: false, msg: 'invalid move'};
        }

        var source = gameBoard[from],
            destination = gameBoard[to],
            playerType= me.getPlayerType(playerId),
            result = {success: false, msg: 'unknown error'};

        // make sure the player owns the source piece
        if (source.owner !== playerType) {
            return {success: false, msg: 'not owned'};
        }

        switch (me.status) {
            case 'waitingForReady':
            case 'waitingForOpponent':
                result = arrangeMove(source, destination);
                break;
            case 'inGame':
                result = inGameMove(playerId, source, destination);
                break;
            default:
                result = {success: false, msg: 'game status invalid'};
        }

        return result;
    };

};





