/**
 * Created with JetBrains PhpStorm.
 * User: root
 * Date: 6/6/14
 * Time: 5:36 AM
 * To change this template use File | Settings | File Templates.
 */

exports.cloakUtils = function () {

    var me = this;

    me.refreshUserLobby = function (user) {
        user.message('refreshLobby', {
            users: user.room.getMembers(true),
            inLobby: user.room.isLobby,
            roomCount: user.room.getMembers().length,
            roomSize: user.room.size
        });
    };

    me.refreshRoomMembersLobby = function (cloak, user) {
        user.room.messageMembers('refreshLobby', {
            users: cloak.getUsers(true),
            inLobby: user.room.isLobby,
            roomCount: user.room.getMembers().length,
            roomSize: user.room.size
        });
    };

    me.getAvailableRooms = function (rooms) {
        var availableRooms = [];
        for (var i=0; i<rooms.length; i++){
            if (rooms[i].users.length<2){
                availableRooms.push(rooms[i]);
            }
        }
        return availableRooms;
    };


    me.generateRoomName = function (suffix) {
        if (!suffix) {
            suffix = Math.floor(Math.random() * 1000000 + 1);
        }
        return 'Game_' + suffix;
    };


    me.getUserRoomInfo = function(userId, room) {
        return {
            id: room.id,
            name: room.name,
            board: room.data.game.getPlayerBoard(userId),
            isPlayerOne: room.data.game.isPlayerOne(userId),
            isPlayerTurn: room.data.game.isPlayerTurn(userId),
            statuses: room.data.game.getPlayerStatuses(userId),
            players: room.data.game.getPlayers()
        };
    };
};