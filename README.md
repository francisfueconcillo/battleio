#Game of the Generals HTML5 Game

Check the game here http://thegenerals.webformitsolutions.com

HTML5 Game implementation of Game of the Generals http://en.wikipedia.org/wiki/Game_of_the_Generals

## Features:
 - Play on any browser
 - Real-time connectivity
 - Lobby, Chatroom,  Private Messaging (work-in-progress)
 - Responsive UI for multiple device screen-size (work-in-progress)

## Technologies:
 - NodeJS
 - SocketIO
 - Cloak - https://github.com/incompl/cloak
 - AngularJS / Angular UI / Bootstrap

## How to run server app:

 - Clone this repo.
 - Go to ./server and Run npm install to install dependencies
 - Run node ./server.js
 - Server app is running at this point

## Setup and serve client app
 - Change client.js CONFIG server URL. Also update the index.html socket.io.js src to get this from server.
 - Serve the client app index.html using Nginx or apache.


Open the client app in your browser.