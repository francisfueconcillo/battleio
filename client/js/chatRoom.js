/**
 * Created with JetBrains PhpStorm.
 * User: root
 * Date: 6/1/14
 * Time: 3:18 PM
 * To change this template use File | Settings | File Templates.
 */

gg.service('ChatRoom', [
    function () {
        'use strict';

        var me = this;
        me.messages = [];

        me.addMessage = function (from, msgData, type) {

            switch (type) {
                case 'challenge':
                    me.messages.push(
                        {type: 'challenge', text: from.name + ' challenges you to a game.', from: 'GG', data: msgData }
                    );
                    break;
                case 'challengeReject':
                    me.messages.push({
                            type: 'system',
                            text: from.name + ' chickened-out from your challenge. You can leave the game of wait for other players to join.',
                            from: 'GG'
                        });
                    break;
            }

        };
    }
]);


gg.controller('chatRoomController', ['$scope', 'Utils', 'CloakService', 'ChatRoom', 'Game', 'Dialogs',
    function ($scope, Utils, CloakService, ChatRoom, Game, Dialogs) {

        $scope.messages = ChatRoom.messages;

        $scope.userMessage = '';

        $scope.send = function (message) {
            if (message !== '') {
                CloakService.sendPublicMessage(message);
                $scope.userMessage = '';
            }
        };

        $scope.showChallengeButtons = function (type) {
            return (type === 'challenge');
        };

        $scope.accept = function (msgIndex, msgData) {

            var actionFn = function () {
                CloakService.joinRoom(msgData.room);
                $scope.messages.splice(msgIndex, 1);
            };

            if (!Game.isInGame()) {
                actionFn();
            } else {
                Dialogs.confirmLeaveGame(Game.game.name, actionFn);
            }
        };

        $scope.reject = function (msgIndex, msgData) {
            $scope.messages.splice(msgIndex, 1);
            CloakService.challengeReject(msgData.id, msgData.room.id);

        };

        $scope.$watch(function () { return ChatRoom.messages; }, function (newVal) {
            $scope.messages = newVal;
        });
    }
]);


gg.run(function($templateCache) {
    $templateCache.put('chatRoom.html',
        '<div class="chatroom" ng-controller="chatRoomController"> ' +
            '<h4>Chat Room</h4>' +
            '<div class="messages">' +
                '<div class="single-message {{ msg.type }}" ng-repeat="msg in messages">' +
                    '<p><strong ng-bind="msg.from"></strong>: <span ng-bind="msg.text"></span></p>' +
                    '<div class="challenge-buttons" ng-show="showChallengeButtons(msg.type)">' +
                        '<button class="btn btn-success btn-sm" ng-click="accept($index, msg.data)">Accept</button>' +
                        '<button class="btn btn-danger btn-sm" ng-click="reject($index, msg.data)">Reject</button>' +
                    '</div>' +
                '</div>' +
            '</div>' +

            '<div class="message-box col-md-12">' +
                '<textarea class="form-control" rows="2" ng-model="userMessage" placeholder="Enter message"></textarea>' +
                '<button class="btn btn-primary btn-lg" ng-click="send(userMessage)">' +
                    '<span class="glyphicon glyphicon-comment"></span>' +
                '</button>' +
            '</div>' +
        '</div>'
    );
});


