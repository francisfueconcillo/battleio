/**
 * Created with JetBrains PhpStorm.
 * User: root
 * Date: 6/1/14
 * Time: 10:14 AM
 * To change this template use File | Settings | File Templates.
 */
gg.service('Utils', [
    function () {
        'use strict';

        var me = this;

        me.hasMethod = function (func) {
            return (typeof func === 'function');
        };

        me.randomNumber = function () {
            return Math.floor(Math.random() * 1000000 + 1);
        };
    }
]);
