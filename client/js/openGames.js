/**
 * Created with JetBrains PhpStorm.
 * User: root
 * Date: 6/6/14
 * Time: 12:19 AM
 * To change this template use File | Settings | File Templates.
 */
gg.controller('openGamesController', ['$scope', 'CloakService', 'Game', 'Dialogs',
    function($scope, CloakService, Game, Dialogs) {

        $scope.openGames =  CloakService.rooms;

        $scope.joinGame = function (game) {
            if (!Game.isInGame()) {
                CloakService.joinRoom(game);
            } else {
                var callBackFn = function () {
                    CloakService.joinRoom(game);
                };

                Dialogs.confirmLeaveGame(Game.game.name, callBackFn);
            }
        };

        $scope.isGameAvailable = function (id, playerCount) {
            var isCurrentGame = false;

            if (!!Game.game) {
                isCurrentGame = (Game.game.id === id);
            }

            return (playerCount < 2 && !isCurrentGame);
        };

        $scope.$watch(function () { return CloakService.rooms; }, function (newVal) {
            $scope.openGames = newVal;
        });

    }
]);


gg.run(function($templateCache) {
    $templateCache.put('openGames.html',
        '<div class="rooms-list" ng-controller="openGamesController">' +
            '<h4>Available Games({{openGames.length}})</h4>' +
            '<div class="gg-list-item" ng-repeat="game in openGames" ng-switch on="isGameAvailable(game.id, game.users.length)">' +
                '<div class="list-name" >' +
                    '<p ng-bind="game.name"></p>' +
                    '<p ng-switch-when="false"> (Playing)</p>'+
                '</div>' +

                '<div class="list-buttons" ng-switch-when="true">' +
                    '<button class="btn btn-primary btn-sm"  ng-click="joinGame(game)">' +
                        '<span class="glyphicon glyphicon-log-in"></span>' +
                    '</button>' +
                '</div>' +
            '</div>' +
        '</div>'
    );
});


