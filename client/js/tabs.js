gg.service('Tabs', [
    function () {
        'use strict';
        var me = this;

        me.list = [
            {title:'Chat Room', type: 'chat', icon: 'glyphicon-comment', isActive:true, template:'chatRoom.html'},
            {title:'Available Players', type: 'players', icon: 'glyphicon-user', isActive:false, template:'onlinePlayers.html'},
            {title:'Open Games', type: 'games', icon: 'glyphicon-tower', isActive:false, template:'openGames.html'},
            {title:'How to Play', type: 'help', icon: 'glyphicon-question-sign', isActive:false, template:'howToPlay.html'},
            {title:'Create Game', type: 'createGame', icon: 'glyphicon-plus', isActive:false, template:'createGame.html'},
        ];

        me.setInactiveAll = function () {
            for (var i=0; i < me.list.length; i++){
                me.list[i].isActive = false;
            }
        };

        me.setActive = function(index){
            me.list[index].isActive = true;
        };

        me.getActive = function(){
            var activeTab = me.list[0];

            for (var i=0; i < me.list.length; i++){
                if (me.list[i].isActive) {
                    activeTab = me.list[i];
                }
            }

            return activeTab;
        };

        me.addGameTab = function(name){
            me.setInactiveAll();
            me.list.push({
                title:name,
                type: 'game',
                icon: 'glyphicon-tower',
                isActive: true,
                template:'gameBoard.html'
            });
        };

        me.addPrivateMsgTab = function(targetUser){
            me.list.push({
                title: 'Message:' + targetUser.name,
                type: 'privateMessage',
                icon: 'glyphicon-envelope',
                isActive: false,
                template:'privateMessage.html',
                data: {
                    targetId: targetUser.id
                }
            });

            return (me.list.length-1);
        };

        me.removeGameTab = function(){
            me.setInactiveAll();
            me.list[0].isActive = true;

            for (var i=0; i < me.list.length; i++){
                if (me.list[i].type === 'game') {
                    me.list.splice(i, 1);
                    break;
                }
            }
        };

        me.removeTab = function(index){
            me.list.splice(index, 1);
        };

        me.removeActiveTab = function(){
            for (var i=0; i < me.list.length; i++){
                if (me.list[i].isActive) {
                    me.list.splice(i, 1);
                }
            }
            me.setInactiveAll();
            me.list[0].isActive = true;
        };

        me.getPrivateMessageTab = function(targetId) {
            var index = null;
            for (var i=0; i < me.list.length; i++){
                if (me.list[i].type === 'privateMessage' && me.list[i].data.targetId === targetId ) {
                    index = i;
                    break;
                }
            }
            return index;
        };

        me.isTabAvailablePM = function (targetId) {
            var isAvailable = false;
            for (var i=0; i < me.list.length; i++){
                if (me.list[i].type === 'privateMessage' && me.list[i].data.targetId === targetId) {
                    isAvailable = true;
                    break;
                }
            }

            return isAvailable;
        };

    }
]);

