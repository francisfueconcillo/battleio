/**
 * Created with JetBrains PhpStorm.
 * User: root
 * Date: 6/14/14
 * Time: 5:38 PM
 * To change this template use File | Settings | File Templates.
 */

gg.controller('createGameController', ['$scope', '$modal', 'Utils', 'CloakService', 'Tabs', 'Game', 'Dialogs',
    function ($scope, $modal, Utils, CloakService, Tabs, Game, Dialogs) {
        'use strict';

        $scope.newGameName = 'newGame_' + Utils.randomNumber();
        $scope.createGame = function (name) {
            if (!Game.isInGame()) {
                CloakService.createRoom(name);
                $scope.newGameName = 'newGame_' + Utils.randomNumber();
            } else {

                var callBackFn = function () {
                    CloakService.createRoom(name);
                };

                Dialogs.confirmLeaveGame(Game.game.name, callBackFn);
            }
        };

    }
]);


gg.run(function ($templateCache) {
    'use strict';

    $templateCache.put('createGame.html',

        '<div class="create-game" ng-controller="createGameController">' +
            '<div class="input-group">' +
                '<h4>Create New Game</h4>' +
                '<div class="input-group">' +
                    '<input type="text" class="form-control" ng-model="newGameName">' +
                    '<span class="input-group-btn ">' +
                        '<button class="btn btn-success" type="button" ng-click="createGame(newGameName)"> + </button>' +
                    '</span>' +
                '</div>'+
            '</div>' +
        '</div>'
    );
});