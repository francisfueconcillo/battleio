gg.service('Game', ['Tabs',
    function (Tabs) {
        'use strict';

        var me = this;

        me.game = null;

        me.init = function (game) {
            console.log('game initialized');
            me.game = game;
            Tabs.addGameTab(game.name);
        };

        me.isInGame = function () {
            return (!!me.game);
        };

        me.updateRoom = function (data) {
            if (me.game) {
                me.game = data;
            }
        };

        me.leaveGame = function () {
            me.game = null;
            Tabs.removeGameTab();
            me.end();
        };

        me.end =  function () {
            me.game = null;
            me.showGameOver('game end');
        };

        me.showGameOver = function (reason) {
            console.log(reason);
        };


    }
]);

