/**
 * Created with JetBrains PhpStorm.
 * User: root
 * Date: 6/8/14
 * Time: 11:31 PM
 * To change this template use File | Settings | File Templates.
 */
gg.controller('gameInfoController', ['$scope', 'Utils', 'CloakService', 'Game', 'Dialogs',
    function ($scope, Utils, CloakService, Game, Dialogs) {
        'use strict';

        $scope.game = Game.game;

        $scope.leaveGame = function () {
            Dialogs.confirmLeaveGame(Game.game.name, function () {});
        };

        $scope.ready = function () {
            CloakService.playerReady();
        };

        $scope.$watch(function () { return Game.game; }, function (newVal) {
            $scope.game = newVal;
        });

        $scope.isInGame = function () {
            if (!Game.game) {  return; }
            return (Game.game.statuses.game === 'inGame' ||
                Game.game.statuses.player === 'waitingForOpponentToReady' ||
                Game.game.statuses.game === 'gameOver');
        };

    }
]);


gg.run(function($templateCache) {
    $templateCache.put('gameInfo.html',
        '<div ng-controller="gameInfoController">' +

            '<div class="game-messages">' +
                '<p><strong ng-bind="game.players.player1.name"></strong> vs <strong ng-bind="game.players.player2.name"></strong></p>' +
                '<p class="status" ng-bind=" game.statuses.player | strings"></p>' +
            '</div>' +

            '<div class="game-controls">' +
                '<button class="btn btn-success btn-sm" ng-click="ready()" ng-hide="isInGame()" >Ready</button>' +
                '<button class="btn btn-danger btn-sm" ng-click="leaveGame()">Leave</button>' +
            '</div>' +
            '<div  >' +

            '</div>' +
        '</div>'
    );
});