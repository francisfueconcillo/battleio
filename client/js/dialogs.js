/**
 * Created with JetBrains PhpStorm.
 * User: root
 * Date: 6/6/14
 * Time: 2:22 AM
 * To change this template use File | Settings | File Templates.
 */
gg.service('Dialogs', ['$rootScope', '$modal', 'CloakService',
    function ($rootScope, $modal, CloakService) {
        'use strict';

        var me = this;

        me.confirmLeaveGame = function (gameName, callBackFn) {
            var confirmModalTpl =
                '<div class="panel-heading">' +
                    '<h3 class="panel-title">Leave Game</h3>' +
                '</div>' +

                '<div class="panel-body">' +
                    '<p>You are currently in a game: <strong ng-bind="gameName"></strong>.<br/><br/>' +
                        'Are you sure you want to leave game? You will lose in the current game.</p>' +

                    '<div class="row-fluid modal-buttons">' +
                        '<div class="col-sm-6">' +
                            '<button class="btn btn-default btn-lg" ng-click="confirm()">Yes</button>' +
                            '<button class="btn btn-primary btn-lg" ng-click="cancel()">Cancel</button>' +
                        '</div>' +
                    '</div>' +
                '</div>';

            var modalInstance = $modal.open({
                template: confirmModalTpl,
                controller: 'confirmLeaveGameController',
                resolve: {
                    modalData: function () {
                        return {
                            gameName: gameName
                        }
                    }
                },
                backdrop: 'static'

            });

            modalInstance.result.then(function () {
                //on ok
                CloakService.leaveRoom();
                callBackFn();
            }, function () {
                //on close,
            });
        };


        me.registerName = function (currentName) {

            var registerNameTpl =
                '<div class="panel-heading">' +
                    '<h3 class="panel-title">Change your name</h3>' +
                '</div>' +

                '<div class="panel-body">' +
                    '<form class="form-horizontal" role="form">' +
                        '<div class="form-group">' +
                            '<label for="username" class="col-sm-3 control-label">Player Name:</label>' +
                            '<div class="col-sm-6">' +
                                '<input type="text" class="form-control" id="username" placeholder="Enter Player Name" ' +
                                'ng-model="username" required maxlength="9"/>' +
                            '</div>' +
                        '</div>' +
                        '<div class="form-group">' +
                            '<div class="col-sm-6 col-sm-offset-3">'+
                            '<button type="submit" class="btn btn-primary btn-lg" ng-click="confirm($event, username)" ' +
                                'ng-class="{disabled:invalidUsername(username)}">OK</button>' +
                            '<button type="submit" class="btn btn-dafault btn-lg" ng-click="cancel($event)">Cancel</button>' +
                            '</div>' +
                        '</div>' +
                    '</form>' +
                '</div>';

            var modalInstance = $modal.open({
                template: registerNameTpl,
                controller: 'registerNameController',
                resolve: {
                    modalData: function () {
                        return {
                            currentName: currentName
                        }
                    }
                },
                backdrop: 'static'
            });

            modalInstance.result.then(function (newName) {
                //on ok
                CloakService.registerUsername(newName);
            }, function () {
                //on close,
            });

        };

    }
]);

gg.controller('confirmLeaveGameController', ['$scope', '$modalInstance', 'modalData',
    function($scope, $modalInstance, modalData) {

        $scope.gameName = modalData.gameName;
        $scope.confirm = function () {
            $modalInstance.close();
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }
]);

gg.controller('registerNameController', ['$scope', 'Utils', 'Game', 'CloakService', '$modalInstance', 'modalData',
    function($scope, Utils, Game, CloakService, $modalInstance, modalData) {

        $scope.username = modalData.currentName;
        $scope.invalidUsername = function (newName) {
            return !newName || newName ==='' || newName.indexOf('Guest') > -1;
        };

        $scope.confirm = function (event, username) {
            event.preventDefault();
            event.stopPropagation();
            if (!$scope.invalidUsername(username)) {
                $modalInstance.close(username);
            }

        };

        $scope.cancel = function (event) {
            event.preventDefault();
            event.stopPropagation();
            $modalInstance.dismiss('cancel');
        };

    }
]);

