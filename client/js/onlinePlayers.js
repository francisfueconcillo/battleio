/**
 * Created with JetBrains PhpStorm.
 * User: root
 * Date: 6/6/14
 * Time: 12:07 AM
 * To change this template use File | Settings | File Templates.
 */
gg.controller('onlinePlayersController', ['$scope', 'CloakService', 'Game', 'Dialogs', 'Utils', 'Tabs', 'PrivateMessage',
    function ($scope, CloakService, Game, Dialogs, Utils, Tabs, PrivateMessage) {

        $scope.onlinePlayers = CloakService.lobbyUsers;

        $scope.isCurrentUser = function (id) {
            return CloakService.getCurrentUserId() === id;
        };

        $scope.challengeUser = function (id) {
            if (!Game.isInGame()) {
                CloakService.challengeUser(id);
            } else {
                var callBackFn = function () {
                    CloakService.challengeUser(id);
                };

                Dialogs.confirmLeaveGame(Game.game.name, callBackFn);
            }
        };

        $scope.messageUser = function (target) {
            var activePMindex = Tabs.getPrivateMessageTab(target.id);

            if (activePMindex === null) {
                activePMindex = Tabs.addPrivateMsgTab(target);
            }

            Tabs.setInactiveAll();
            Tabs.setActive(activePMindex);
            PrivateMessage.setCurrentTarget(target.id);
        };

        $scope.$watch(function () { return CloakService.lobbyUsers; }, function (newVal) {
            $scope.onlinePlayers = newVal;
        });

    }
]);


gg.run(function($templateCache) {
    $templateCache.put('onlinePlayers.html',
        '<div class="users-list" ng-controller="onlinePlayersController">' +
            '<h4>Available Players({{onlinePlayers.length}})</h4>' +
            '<div class="gg-list-item" ng-repeat="player in onlinePlayers" ng-switch on="isCurrentUser(player.id)">' +
                '<div class="list-name" >' +
                    '<p ng-bind="player.name"></p>' +
                    '<p ng-switch-when="true"> (You)</p>'+
                '</div>' +

                '<div class="list-buttons" ng-switch-when="false">' +
                    '<button class="btn btn-success btn-sm" ng-click="messageUser(player)">' +
                        '<span class="glyphicon glyphicon-envelope"></span>' +
                    '</button>' +

                    '<button class="btn btn-danger btn-sm"  ng-click="challengeUser(player.id)">' +
                        '<span class="glyphicon glyphicon-flash"></span>' +
                    '</button>' +
                '</div>' +

            '</div>' +
        '</div>'
    );
});


