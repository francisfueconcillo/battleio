/**
 * Created with JetBrains PhpStorm.
 * User: root
 * Date: 6/1/14
 * Time: 11:54 AM
 * To change this template use File | Settings | File Templates.
 */
gg.service('CloakService', ['$rootScope', 'Utils', 'Game', 'ChatRoom', 'Tabs', 'PrivateMessage',
    function ($rootScope, Utils, Game, ChatRoom, Tabs, PrivateMessage) {
        'use strict';

        var me = this,
            gameEvents = {},
            serverEvents = {
                begin: function () {
                    console.log('server begin');
                    cloak.message('getCurrentPlayer');
                    $rootScope.$apply();
                },
                roomMemberLeft: function (user) {
                    console.log('room member left', user);
                    console.log('Removing you from the room because the other player disconnected.');
                    // The other player dropped, so we need to stop the game and show retGame.showGameOver('The other player disconnected!');
                    cloak.message('leaveRoom');
                }
            },
            clientEvents = {
                registerUsernameResponse: function (success) {
                    console.log(success ? 'username registered' : 'username failed');
                    // if we registered a username, try to join the lobby
                    if (success) {
                        cloak.message('getCurrentPlayer');
                    }
                },
                getCurrentPlayerResponse: function (data) {
                    console.log(data);
                    me.currentUser = data;
                    $rootScope.$apply();

                },
                joinLobbyResponse: function (data) {
                    me.currentUser = data;
                    cloak.message('listUsers');
                    cloak.message('listRooms');
                    $rootScope.$apply();
                },
                refreshLobby: function (data) {
                    me.lobbyUsers = data.users;
                    me.isLobby = data.inLobby;
                    $rootScope.$apply();
                },
                listRooms: function (rooms) {
                    console.log('listRooms:', rooms);
                    me.rooms = rooms;
                    $rootScope.$apply();
                },
                roomCreated: function (result) {
                    console.log(result.success ? 'room join success' : 'room join failure');
                    if (result.success) {
                        Game.init(result.room);
                        $rootScope.$apply();
                    }
                },
                joinRoomResponse: function (result) {
                    if (result.success) {
                        Game.init(result.room);
                        $rootScope.$apply();
                    }
                },
                leaveRoomResponse: function () {
                    Game.leaveGame();
                },
                publicMessageResponse: function (message) {
                    ChatRoom.messages.push(message);
                    $rootScope.$apply();
                },
                allPublicMessages: function (messages) {
                    ChatRoom.messages = messages;
                    $rootScope.$apply();
                },
                challengeUser: function (result) {
                    ChatRoom.addMessage({id: result.id, name: result.name}, result, 'challenge');
                    $rootScope.$apply();

                },
                challengeReject: function (result) {
                    if (!Game.game) { return; }

                    if (Game.game.id === result.roomId) {
                        ChatRoom.addMessage({id: result.id, name: result.name}, result, 'challengeReject');
                        $rootScope.$apply();
                    }
                },
                privateMessage: function (result) {
                    var from = {
                        id: result.id,
                        name: result.name
                    };
                    PrivateMessage.addMessage(result);
                    $rootScope.$apply();
                },
                updateGame: function (result) {
                    Game.updateRoom(result.room);
                    $rootScope.$apply();
                }

            };

        me.rooms = [];
        me.lobbyUsers = [];
        me.isLobby = false;
        me.currentUser = {
            id: null,
            name: null
        };

        me.getCurrentUserId = function () {
            var id = null;

            if (Utils.hasMethod(cloak.currentUser)) {
                id = cloak.currentUser();
            }
            return id;
        };

        me.registerUsername = function (username) {
            cloak.message('registerUsername', {
                username: username
            });
        };

        me.createRoom = function (roomName) {
            cloak.message('createRoom', {
                name: roomName
            });
        };

        me.joinRoom = function (room) {
            cloak.message('joinRoom', room);
        };

        me.leaveRoom = function () {
            cloak.message('leaveRoom');
        };


        me.sendPublicMessage = function (message) {
            cloak.message('publicMessage', {
                message: message
            });
        };

        me.challengeUser = function (targetId) {
            cloak.message('challengeUser', {
                targetId: targetId
            });
        };

        me.privateMessage = function (targetId, message) {
            cloak.message('privateMessage', {
                targetId: targetId,
                message: message
            });
        };

        me.challengeReject = function (fromId, roomId) {
            cloak.message('challengeReject', {
                targetId: fromId,
                roomId: roomId
            });
        };

        me.playerReady = function () {
            cloak.message('playerReady');
        };

        me.playerMove = function (from, to) {
            cloak.message('playerMove', {
                playerId: me.getCurrentUserId(),
                from: from,
                to: to
            });
        };


        cloak.configure({
            messages: {
                'registerUsernameResponse': clientEvents.registerUsernameResponse,
                'getCurrentPlayerResponse': clientEvents.getCurrentPlayerResponse,
                'joinLobbyResponse': clientEvents.joinLobbyResponse,
                'refreshLobby': clientEvents.refreshLobby,
                'listRooms': clientEvents.listRooms,
                'roomCreated': clientEvents.roomCreated,
                'joinRoomResponse': clientEvents.joinRoomResponse,
                'leaveRoomResponse': clientEvents.leaveRoomResponse,
                'publicMessageResponse': clientEvents.publicMessageResponse,
                'challengeUser': clientEvents.challengeUser,
                'privateMessage': clientEvents.privateMessage,
                'allPublicMessages': clientEvents.allPublicMessages,
                'challengeReject': clientEvents.challengeReject,
                'opponentJoinRoom': clientEvents.updateGame,
                'updateGame': clientEvents.updateGame

            },

            serverEvents: {
                begin: serverEvents.begin,
                roomMemberLeft: serverEvents.roomMemberLeft
            }
        });
        cloak.run(CONFIG.server);
    }
]);

