/**
 * Created with JetBrains PhpStorm.
 * User: root
 * Date: 6/6/14
 * Time: 12:08 AM
 * To change this template use File | Settings | File Templates.
 */
gg.controller('mainPanelController', ['$scope', 'CloakService', 'Tabs', 'Dialogs', 'Game', 'PrivateMessage', '$window',
    function($scope, CloakService, Tabs, Dialogs,Game, PrivateMessage, $window) {


        $scope.mainPanelTemplate = Tabs.getActive().template;
        $scope.tabs = Tabs.list;
        $scope.showTab = function (index) {
            Tabs.setInactiveAll();
            Tabs.setActive(index);

            if (Tabs.list[index].type === 'privateMessage') {
                PrivateMessage.currentTarget = Tabs.list[index].data.targetId;
            }
            $scope.mainPanelTemplate = Tabs.getActive().template;
        };

        $scope.getButtonClass = function (tab) {
            var windoWidth = $window.innerWidth;
            return {
                'btn-success': tab.isActive,
                'btn-lg': windoWidth > 480,
                'btn-sm': windoWidth < 480
            };
        };

        $scope.$watch(function(){ return CloakService.currentUser.name; }, function(newVal){
            if (newVal && newVal.indexOf('Guest') > -1){
                Dialogs.registerName(newVal);
            }
        });

        angular.element($window).bind('resize', function () {
            $scope.$apply();
        });

        $scope.$watch(Tabs.getActive, function(newVal) {
            $scope.mainPanelTemplate = newVal.template;
        });

        $scope.$watch(function(){ return Tabs.list}, function(newVal) {
            $scope.tabs = newVal;
        });
    }
]);


gg.run(function($templateCache) {
    $templateCache.put('mainPanel.html',
        '<div class="main-panel col-md-12" ng-controller="mainPanelController">' +
            '<div class="tab-container">' +
                '<button class="btn btn-default" ng-repeat="tab in tabs" ng-class="getButtonClass(tab)"   ng-click="showTab($index)">' +
                    '<span class="glyphicon {{tab.icon}}"></span>' +
                '</button>' +
            '</div>' +
            '<div class="main-panel-content" ng-include="mainPanelTemplate"></div>' +
        '</div>'
    );
});

