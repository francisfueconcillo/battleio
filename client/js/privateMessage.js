/**
 * Created with JetBrains PhpStorm.
 * User: root
 * Date: 6/8/14
 * Time: 6:34 PM
 * To change this template use File | Settings | File Templates.
 */
gg.service('PrivateMessage', [ 'Tabs',
    function (Tabs) {
        'use strict';

        var me = this,
            allPMessages = {};

        me.currentTarget = null;

        me.setCurrentTarget = function (targetId) {
            if (!allPMessages.hasOwnProperty(targetId)) {
                allPMessages[targetId] = [];
            }
            me.currentTarget = targetId;
        };

        me.getCurrentMessages = function () {
            var messages = [];

            if (allPMessages.hasOwnProperty(me.currentTarget)) {
                messages = allPMessages[me.currentTarget];
            }
            return messages;
        };

        me.addMessage = function (msgData) {
            if (!allPMessages.hasOwnProperty(msgData.id)) {
                allPMessages[msgData.id] = [];
            }

            //create a new tab if no available for targetId
            if (!Tabs.isTabAvailablePM(msgData.id)){
                Tabs.addPrivateMsgTab(msgData);
            }

            allPMessages[msgData.id].push({
                type: 'privateMessage',
                text: msgData.message,
                from: msgData.name
            });
        };

        me.addOwnMessage = function (message) {
            allPMessages[me.currentTarget].push({
                type: 'privateMessage',
                text: message,
                from: '(You)'
            });
        };
    }
]);


gg.controller('privateMessageController', ['$scope', 'Utils', 'CloakService', 'ChatRoom', 'Game', 'Dialogs', 'PrivateMessage', 'Tabs',
    function ($scope, Utils, CloakService, ChatRoom, Game, Dialogs, PrivateMessage, Tabs) {

        $scope.messages = PrivateMessage.getCurrentMessages();
        $scope.userMessage = '';

        $scope.send = function (message) {
            if (message !== '') {
                CloakService.privateMessage(PrivateMessage.currentTarget, message);
                PrivateMessage.addOwnMessage(message);
                $scope.userMessage = '';
            }
        };

        $scope.closeTab = function (){
            Tabs.removeActiveTab();
        };

        $scope.$watch(PrivateMessage.getCurrentMessages, function (newVal) {
            $scope.messages = newVal;
        });
    }
]);

gg.run(function($templateCache) {
    $templateCache.put('privateMessage.html',
        '<div class="private-message" ng-controller="privateMessageController"> ' +
            '<div class="close-button">' +
                '<button class="btn btn-danger btn-md" ng-click="closeTab()">' +
                    '<span class="glyphicon glyphicon glyphicon-remove"></span> <span class="label">Close</span>' +
                '</button>' +
            '</div>' +

            '<div class="messages">' +
                '<div class="single-message {{ msg.type }}" ng-repeat="msg in messages">' +
                    '<p><strong ng-bind="msg.from"></strong>: <span ng-bind="msg.text"></span></p>' +
                '</div>' +
            '</div>' +

            '<div class="message-box">' +
                '<textarea class="form-control" rows="2" ng-model="userMessage" placeholder="Enter message"></textarea>' +
                '<button class="btn btn-primary btn-lg" ng-click="send(userMessage)">' +
                    '<span class="glyphicon glyphicon-comment"></span>' +
                '</button>' +
            '</div>' +
        '</div>'
    );
});


