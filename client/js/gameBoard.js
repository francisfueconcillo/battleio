/**
 * Created with JetBrains PhpStorm.
 * User: root
 * Date: 6/1/14
 * Time: 3:18 PM
 * To change this template use File | Settings | File Templates.
 */
gg.controller('gameBoardController', ['$scope', 'Utils', 'CloakService', 'Game', 'Dialogs',
    function($scope, Utils, CloakService, Game, Dialogs) {

        var me = this;

        me.currentSelectedTile = null;
        $scope.board = [];

        $scope.cellClick = function (tile) {
            console.log(tile, me.currentSelectedTile);
            if (!tile.content && !me.currentSelectedTile) { return; }

            if (!me.currentSelectedTile) {
                me.currentSelectedTile = tile;
            } else {
                CloakService.playerMove(me.currentSelectedTile.location, tile.location);
                me.currentSelectedTile = null;

            }
        };

        $scope.isTileSelected = function (tile) {
            return me.currentSelectedTile === tile;
        };

        $scope.getTileClass = function (tile) {
            return {
                selected: $scope.isTileSelected(tile),
                occupied: !!tile.content
            };
        };

        $scope.getPieceClass = function (tile) {
            var tileContentCls = {};

            if (!Game.game) {  return; }

            if (!!tile.owner) {
                tileContentCls[tile.owner] = true;
            }

            if (!!tile.content) {
                if (tile.content.hasOwnProperty('type')) {
                    tileContentCls[tile.content.type] = true;
                }
            }

            return tileContentCls;
        };

        $scope.$watch(function () { return Game.game; }, function (newVal) {
            console.log(newVal);
            if (!newVal) {  return; }
            $scope.board = newVal.board;
        });
    }
]);

gg.run(function($templateCache) {
    $templateCache.put('gameBoard.html',
        '<div class="board" ng-controller="gameBoardController"> ' +
            '<div ng-repeat="tile in board" class="tile" ng-class="getTileClass(tile)" ng-click="cellClick(tile)" ng-switch on="!!tile.owner">' +
                '<div class="tile-content" ng-class="getPieceClass(tile)" ng-switch-when="true">' +
                    '<div class="name">' +
                        '<p ng-bind="tile.content.name"></p>' +
                    '</div>' +
                '</div>' +
            '</div>' +

        '</div>' +
        '<div class="game-info" ng-include="\'gameInfo.html\'"></div>'
    );
});


