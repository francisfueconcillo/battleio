gg.filter('strings', [
    function () {
        'use strict';

        var STRINGS = {
            waitingForOpponent: 'Waiting for opponent...',
            waitingForReady: 'Both players must be ready.',
            arrangePieces: 'Prepare for battle.',
            waitingForOpponentToReady: 'Wait for opponent to ready.',
            inGame: 'Game starts.',
            playerTurn: 'Make a move.',
            opponentTurn: 'Not yet your turn.',
            gameOverWin: 'You win!',
            gameOverLose: 'You lose!',
            gameOver: 'Game Over'
        };

        return function (key) {
            var actualString = key;
            if (STRINGS.hasOwnProperty(key)) {
                actualString = STRINGS[key];            }
            return actualString;
        };
    }
]);


