/**
 * Created with JetBrains PhpStorm.
 * User: root
 * Date: 6/14/14
 * Time: 9:18 AM
 * To change this template use File | Settings | File Templates.
 */

var Game = require('../../server/game').Game,
    gameObject;



describe('initBoard', function(){

    beforeEach(function() {
        gameObject = new Game ();
        gameObject.initBoard({id: 12345, name: 'xxx'});
    });

    it('should set player1 and player and game statuses', function(){
        expect (gameObject.player1.id).toEqual(12345);
        expect (gameObject.player1.name).toEqual('xxx');
        expect (gameObject.player1.status).toEqual('arrangePieces');
        expect (gameObject.status).toEqual('waitingForOpponent');
    });
});

describe('playerTwoJoined', function(){
    beforeEach(function() {
        gameObject = new Game ();
        gameObject.initBoard({id: 12345, name: 'xxx'});
        gameObject.playerTwoJoined({id: 67890, name: 'yyy'});
    });

    it('should set player1 and player and game statuses', function(){
        expect (gameObject.player2.id).toEqual(67890);
        expect (gameObject.player2.name).toEqual('yyy');
        expect (gameObject.player2.status).toEqual('arrangePieces');
        expect (gameObject.status).toEqual('waitingForReady');
    });
});

describe('isPlayerOne', function(){
    beforeEach(function() {
        gameObject = new Game ();
        gameObject.player1 = {id: 12345}
        gameObject.player2 = {id: 67890}
    });

    it('should check if playerID is player 1 or not', function(){
        expect (gameObject.isPlayerOne(12345)).toBeTruthy();
        expect (gameObject.isPlayerOne(67890)).toBeFalsy();
        expect (gameObject.isPlayerOne(1111)).toBeFalsy();
    });
});



describe('getPlayerBoard', function(){
    beforeEach(function() {
        gameObject = new Game ();

    });

    it('should return a board that only shows content owned by player', function(){
        //TODO
    });
});


describe('getPlayerStatuses', function(){
    beforeEach(function() {
        gameObject = new Game ();
        gameObject.player1 = {id: 12345, status: 'aaa'};
        gameObject.player2 = {id: 67890, status: 'bbb'};
        gameObject.status = 'xxx';

    });

    it('should return game and player statuses', function(){
        expect (gameObject.getPlayerStatuses(12345)).toEqual({
            game: 'xxx',
            player: 'aaa'
        });

        expect (gameObject.getPlayerStatuses(67890)).toEqual({
            game: 'xxx',
            player: 'bbb'
        })

    });
});

describe('playerMove - inGame allowed moves', function(){
    beforeEach(function() {
        gameObject = new Game ();
        gameObject.initBoard({id: 12345, status: 'aaa'});
        gameObject.playerTwoJoined({id: 67890, status: 'bbb'});
        gameObject.status = 'inGame';
    });

    it('should not allow invalid moves', function(){
        var invalid = {success: false, msg: 'invalid move'};

        spyOn(gameObject, 'isPlayerTurn').andCallFake(function() { return true;});

        //empty both sides
        gameObject.playerMove(12345, 48, 40);
        gameObject.playerMove(12345, 50, 42);

        expect(gameObject.playerMove(12345, 49, 72)).toEqual(invalid);
        expect(gameObject.playerMove(12345, 49, -1)).toEqual(invalid);

        expect(gameObject.playerMove(12345, 49, 47)).toEqual(invalid);
        expect(gameObject.playerMove(12345, 49, 51)).toEqual(invalid);

        expect(gameObject.playerMove(12345, 49, 33)).toEqual(invalid);
        expect(gameObject.playerMove(12345, 49, 55)).toEqual(invalid);

    });

    it('should not move to next row when tiles on the borders of the board', function(){
        var invalid = {success: false, msg: 'invalid move'};

        spyOn(gameObject, 'isPlayerTurn').andCallFake(function() { return true;});

        expect(gameObject.playerMove(12345, 48, 47)).toEqual(invalid);

        gameObject.playerMove(12345, 55, 47);
        gameObject.playerMove(12345, 48, 40);

        expect(gameObject.playerMove(12345, 47, 48)).toEqual(invalid);
    });

    it('should allow move left and right', function(){
        var valid = {success: true, msg: 'move done.'};
        spyOn(gameObject, 'isPlayerTurn').andCallFake(function() { return true;});

        //empty both sides
        gameObject.playerMove(12345, 48, 40);
        gameObject.playerMove(12345, 50, 42);

        expect(gameObject.playerMove(12345, 49, 48)).toEqual(valid);
        expect(gameObject.playerMove(12345, 48, 49)).toEqual(valid);
    });

    it('should allow move up and down', function(){
        var valid = {success: true, msg: 'move done.'};
        spyOn(gameObject, 'isPlayerTurn').andCallFake(function() { return true;});

        expect(gameObject.playerMove(12345, 49, 41)).toEqual(valid);
        expect(gameObject.playerMove(12345, 41, 49)).toEqual(valid);
    });

    it('should not allow move if not yet players turn', function(){
        var invalid = {success: false, msg: 'not yet your turn'};
        gameObject.playerMove(12345, 49, 41);
        expect(gameObject.playerMove(12345, 50, 42)).toEqual(invalid);
    });

    it('should not allow move if destination is occupied by same player', function(){
        var invalid = {success: false, msg: 'invalid move'};
        spyOn(gameObject, 'isPlayerTurn').andCallFake(function() { return true;});
        expect(gameObject.playerMove(12345, 48, 49)).toEqual(invalid);
    });

});

describe('challengeMove', function(){
    var source, destination;
    beforeEach(function() {
        gameObject = new Game ();
    });

    it('should lose both players when same rank - not flag', function(){
        source = {owner: 'player1', content: {rank: 70}};
        destination = {owner: 'player2', content: {rank: 70}};

        gameObject.challengeMove(source, destination);

        expect(source.owner).toEqual(null);
        expect(source.content).toEqual(null);
        expect(destination.owner).toEqual(null);
        expect(destination.content).toEqual(null);
    });

    it('should win player 1 when flag vs opponent flag', function(){
        source = {owner: 'player1', content: {rank: 1}};
        destination = {owner: 'player2', content: {rank: 1}};

        gameObject.challengeMove(source, destination);

        expect(source.owner).toEqual(null);
        expect(source.content).toEqual(null);
        expect(destination.owner).toEqual('player1');
        expect(destination.content).toEqual({rank: 1});

    });

    it('should win when player1 and heigher rank - not spy  ', function(){

        source = {owner: 'player1', content: {rank: 150}};
        destination = {owner: 'player2', content: {rank: 70}};

        gameObject.challengeMove(source, destination);

        expect(source.owner).toEqual(null);
        expect(source.content).toEqual(null);
        expect(destination.owner).toEqual('player1');
        expect(destination.content).toEqual({rank: 150});

    });

    it('should lose when player1 attack is lower rank - not spy  ', function(){

        source = {owner: 'player1', content: {rank: 70}};
        destination = {owner: 'player2', content: {rank: 150}};

        gameObject.challengeMove(source, destination);

        expect(source.owner).toEqual(null);
        expect(source.content).toEqual(null);
        expect(destination.owner).toEqual('player2');
        expect(destination.content).toEqual({rank: 150});

    });

    it('should lose when player1 defensive is lower rank - not spy  ', function(){

        source = {owner: 'player2', content: {rank: 150}};
        destination = {owner: 'player1', content: {rank: 70}};

        gameObject.challengeMove(source, destination);

        expect(source.owner).toEqual(null);
        expect(source.content).toEqual(null);
        expect(destination.owner).toEqual('player2');
        expect(destination.content).toEqual({rank: 150});

    });


    it('should win when player1 is spy and opponent is not spy/private', function(){

        source = {owner: 'player1', content: {rank: 200}};
        destination = {owner: 'player2', content: {rank: 150}};

        gameObject.challengeMove(source, destination);

        expect(source.owner).toEqual(null);
        expect(source.content).toEqual(null);
        expect(destination.owner).toEqual('player1');
        expect(destination.content).toEqual({rank: 200});

    });

    it('should lose when player1 is spy and opponent is private', function(){

        source = {owner: 'player1', content: {rank: 200}};
        destination = {owner: 'player2', content: {rank: 30}};

        gameObject.challengeMove(source, destination);

        expect(source.owner).toEqual(null);
        expect(source.content).toEqual(null);
        expect(destination.owner).toEqual('player2');
        expect(destination.content).toEqual({rank: 30});
    });

    it('should win when player1 is private and opponent is spy', function(){

        source = {owner: 'player1', content: {rank: 30}};
        destination = {owner: 'player2', content: {rank: 200}};

        gameObject.challengeMove(source, destination);

        expect(source.owner).toEqual(null);
        expect(source.content).toEqual(null);
        expect(destination.owner).toEqual('player1');
        expect(destination.content).toEqual({rank: 30});
    });

    it('should gameOver when flag is captured.', function(){
        //TODO

    });





});










