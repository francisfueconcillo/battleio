//command: karma start karma-conf.js [iframe_version]

//https://github.com/karma-runner/karma/blob/v0.10.2/config.tpl.js

module.exports = function (config) {

    config.set({

        // base path, that will be used to resolve files and exclude
        basePath: '../..',

        // frameworks to use
        frameworks: ['jasmine'],

        // list of files / patterns to load in the browser
        files: [
            'client/lib/underscore-min.js',
            'client/lib/cloak-client.js',
            'client/lib/angular.min.js',
            'client/lib/ui-bootstrap-custom-tpls-0.10.0.js',
            'client/js/client.js',
            'client/js/*.js',
            'test-ui/lib/angular-mocks.js',
            'test-ui/lib/fakeCloakService.js',
            'test-ui/unit/*.js'
        ],

        preprocessors: {
            '**/client/js/*.js': 'coverage'
        },

        // list of files to exclude
        exclude: ['client/js/cloakService.js'],

        reporters: ['junit', 'dots', 'coverage'],

        junitReporter: {
            outputFile: 'test-ui/out/unit.xml',
            suite: 'unit'
        },

        coverageReporter: {
            type: 'lcov',
            dir: 'test-ui/out/coverage/',
            file: 'coverage.html'
        },

        // web server port
        port: 9876,

        // enable / disable colors in the output (reporters and logs)
        colors: true,


        // level of logging
        // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
        logLevel: config.LOG_INFO,


        // enable / disable watching file and executing tests whenever any file changes
        autoWatch: true,


        // Start these browsers, currently available:
        // - Chrome
        // - ChromeCanary
        // - Firefox
        // - Opera
        // - Safari (only Mac)
        // - PhantomJS
        // - IE (only Windows)
        browsers: [],


        // If browser does not capture in given timeout [ms], kill it
        captureTimeout: 60000,


        // Continuous Integration mode
        // if true, it capture browsers, run tests and exit
        singleRun: false
    });
};
