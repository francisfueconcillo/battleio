describe('openGames Tests', function () {
    'use strict';

    var scope, ctrl;
    beforeEach(module('gg'));
    beforeEach(inject(
        function ($httpBackend, $rootScope, $controller) {
            scope = $rootScope.$new();
            ctrl = $controller('openGamesController', {$scope: scope});
        }
    ));

    describe('joinGame() Tests', function () {
        it('should call CloakService.joinRoom when not in game', inject(
            function (CloakService, Game) {
                spyOn(Game, 'isInGame').andCallFake(function () { return false; });
                spyOn(CloakService, 'joinRoom').andCallFake(function () {});
                scope.joinGame({some: 'data'});
                expect(CloakService.joinRoom).toHaveBeenCalledWith({some: 'data'});

            }
        ));

    });

});

